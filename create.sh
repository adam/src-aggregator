#!/bin/bash

usage() { echo "Usage: $0 -c <string> | -h" 1>&2; }

OPTIND=1

while getopts hc: opt; do
    case $opt in
        h)
            usage >&1
            exit 0
            ;;
        c)  creates=$OPTARG
            ;;
        *)
            usage
            exit 1
            ;;
    esac
done
shift "$((OPTIND-1))" 

for f in `ls -1 $creates`
do
        impala-shell -f $creates/$f
done
