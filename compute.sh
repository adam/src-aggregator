#!/bin/bash

usage() { echo "Usage: $0 -d <int> -m <int> -y <int> -t <string> -c <string> | -h" 1>&2; }


OPTIND=1

while getopts hd:m:y:t:c: opt; do
    case $opt in
        h)
            usage >&1
            exit 0
            ;;
        d)  day=$OPTARG
            ;;
        m)  month=$OPTARG
            ;;
        y)  year=$OPTARG
            ;;
        t)  table=$OPTARG
            ;;
        c)  ctpls=$OPTARG
            ;;
        *)
            usage
            exit 1
            ;;
    esac
done
shift "$((OPTIND-1))" 

for f in `ls -1 $ctpls`
do
        impala-shell -f $ctpls/$f --var=table=$table --var=year=$year --var=month=$month --var=day=$day
done
