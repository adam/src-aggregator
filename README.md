# src aggregator

Impala SQL queries to compute features for DNS resolvers

### Repository content ###

#### Directories with SQL query templates ####
  * `compute_templates` - SQL query templates for feature computing
  * `compute_templates_extra` - SQL query templates for feature computing (extra features - not in usage)
  * `create_table` - SQL query templates for creating Impala tables
  * `create_table_extra` - SQL query templates for creating Impala tables (extra features - not in usage)

#### Bash scripts ####
  * `create.sh` - a script for creating Impala tables
  * `delete.sh` - a script for deleting Impala tables
  * `compute.sh` - a script for feature computing

## Installation

### Create database ###
```console
impala-shell -q 'CREATE DATABASE rr'
```

### Create tables ###

In order to create tables run `create.sh` script with parameters:

  * `-c` - path to the directory with SQL templates (e.g.: `create_templates`)

For example:

```console
./create.sh -c create_templates
```

## Computing features

In order to compute features run `compute.sh` script with parameters:

  * `-d` - day
  * `-m` - month
  * `-y` - year
  * `-t` - input table
  * `-c` - path to the directory with SQL templates (e.g.: `compute_templates`)

For example:

```console
./compute.sh -d 1 -m 10 -y 2018 -t dns.queries -c compute_templates
```

