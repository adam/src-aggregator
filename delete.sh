#!/bin/bash

usage() { echo "Usage: $0 -d <int> -m <int> -y <int> -b <string> | -h" 1>&2; }


OPTIND=1

while getopts hd:m:y:b: opt; do
    case $opt in
        h)
            usage >&1
            exit 0
            ;;
        d)  day=$OPTARG
            ;;
        m)  month=$OPTARG
            ;;
        y)  year=$OPTARG
            ;;
        b)  db=$OPTARG
            ;;
        *)
            usage
            exit 1
            ;;
    esac
done
shift "$((OPTIND-1))" 

tables=`impala-shell -q "show tables in $db" -B 2> /dev/null`

for t in $tables
do
        impala-shell -q "ALTER TABLE $db.$t DROP PARTITION (year=$year AND month=$month AND day=$day)"
done
